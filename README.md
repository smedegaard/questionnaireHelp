# Hvordan bruge Atom til at skrive spørgsmål i Wpf Questionnaire

## Installere Atom text editor
Gå til https://atom.io/. Installations linket er på forsiden. Når filen er downloadet åbnes filen og du trykker "next" til installationen er færdig.

## Konfigurere Atom

### Snippets
For at gøre det enklere at skrive spørgsmålene bruger vi "snippets". Det vil sige kode-maler. I Atom vælger du `Filer` -> `Snippets...`. Det åbner filen `snippets.cson`. Tilføj følgende i bunden af filen.

```
# Your snippets
#
# Atom snippets allow you to enter a simple prefix in the editor and hit tab to
# expand the prefix into a larger code block with templated values.
#
# You can create a new snippet in this file by typing "snip" and then hitting
# tab.
#
# An example CoffeeScript snippet to expand log to console.log:
#
# '.source.coffee':
#   'Console log':
#     'prefix': 'log'
#     'body': 'console.log $1'
#
# Each scope (e.g. '.source.coffee' above) can only be declared once.
#
# This file uses CoffeeScript Object Notation (CSON).
# If you are unfamiliar with CSON, you can read more about it in the
# Atom Flight Manual:
# http://flight-manual.atom.io/using-atom/sections/basic-customization/#_cson

'.text.xml':
  'Binary Question':
    'prefix': 'bin'
    'body': """
    <wpfQuestionnaire:BinaryQuestion
      QuestionNumber="$1"
      QuestionText="$2"
      VariableName="$3" />

    $4
    """
  'Trinary Question':
    'prefix': 'tri'
    'body':"""
    <wpfQuestionnaire:TrinaryQuestion
      QuestionNumber="$1"
      QuestionText="$2"
      VariableName="$3" />

    $4
    """
  'Multiple Choice Question':
    'prefix': 'mul'
    'body': """
    <wpfQuestionnaire:MultipleChoiceQuestion
      QuestionNumber="$1"
      QuestionText="$2"
      VariableName="$3">

    $4
    </wpfQuestionnaire:MultipleChoiceQuestion>
     """
  'Multiple Choice Option':
    'prefix': 'mulop'
    'body': """
    <wpfQuestionnaire:MultipleChoiceOption
      OptionText="$1"
      OptionType="$2" />

  $3
    """
  'Multiple Choice combobox':
    'prefix': 'mulcom'
    'body': """
    <wpfQuestionnaire:MultipleChoiceOption
      OptionText="$1"
      OptionType="combobox"
      ComboBoxText="$2"
      ComboBoxItems="{Binding }" />

    $3
    """
  'Multiple Choice Checkbox':
    'prefix': 'mulcheck'
    'body': """
    <wpfQuestionnaire:MultipleChoiceOption
      OptionType="checkbox"
      OptionText="$1"
      OptionVariableName="$2"/>

    $3
    """
  'Text Question':
    'prefix': 'txt'
    'body': """
    <wpfQuestionnaire:TextQuestion
      QuestionNumber="$1"
      QuestionText="$2"
      VariableName="$3"
      OnlyNumbers="${4:false}" />

    $5
    """

```

### Set-syntax
For at kunne sætte syntaksen på nye filer på en enkel måde installerer vi Atom-pakken `Set Syntax` (https://atom.io/packages/set-syntax).
Den enkleste måde at installerer pakken på er at trykke `ctrl` + `,`. Dette åbner "settings" i Atom. Vælg "install" og skriv "set+syntax" i søgefeltet. Trykk på `Install` knappen ved pakken "set-syntax" (Øverste mulighed).


## Bruge snippets i Atom
For at bruge de snippets vi har tilføjet til `snippets.cson` skal Atom vide at det er en .xml fil vi arbejder med. Når du åbner en ny fil i Atom har filen ikke et filnavn (da man kan arbejde med mange forskellige filtyper i Atom). Det kan enten løses ved at man lagre filen som en .xml fil. Vælg `Filer` -> `Lagre som...` og giv filen et navn som slutter på `.xml`. Eksempelvis `skjema1.xml`.

Hvis pakken "set-syntax" er installeret kan man trykke `shift` + `ctrl` + `p`. Dett åbner et testfelt i toppen af Atom. I dette tekstfelt skriver du "xml" og vælger øverste mulighed "set syntax: XML" ved at trykke `enter`.

Når filen eller lagret eller syntaksen er sat med set-syntax kan vi begynde at bruge vores snippets.

Vi har defineret 7 snippets:

* `bin` : ja / nej
* `tri` : ja / nej / ved ikke
* `mul` : multiple choice
* `mulop` : multiple choice svarmulighed (ekskluderende muligheder)
* `mulcom` : multiple choice rullegardin (ekskluderende)
* `mulckeck` : multiplechoice checkbokse (ikke ekskludernede)
* `txt`: Tekstspørgsmål. Tal hvis `OnlyNumbers` sættes til `true`

# WPF Questionnaire dokumentation
En mere udførlig dokumentation af alle spørgsmålstyper findes på engelsk https://gitlab.com/smedegaard/wpf-questionnaire
